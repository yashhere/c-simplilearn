#ifndef BIRD_ENTRY_INTERFACE_H
#define BIRD_ENTRY_INTERFACE_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

typedef struct
{
    int reserved : 12;
    unsigned int year : 11;
    unsigned int month : 4;
    unsigned int day : 5;
    short id;
    short number_observed;
} bird_entry;

typedef struct
{
    unsigned size;
    bird_entry *data;
} stats;

int compare_id(const void *a, const void *b);
void read_observation_file(stats *data);
void print_stats(bird_entry *bird_data, int size);

#endif