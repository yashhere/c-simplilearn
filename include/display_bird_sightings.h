#ifndef DISPLAY_BIRD_SIGHTINGS_H
#define DISPLAY_BIRD_SIGHTINGS_H

#include "main.h"
#include "bird.h"
#include "bird_entry.h"
#include "menu.h"

int compare_sightings(const void *a, const void *b);
void filter_observations_by_id(stats *obs_data, int id, stats *filtered_birds);
void display_bird_sightings(birds *bird_data, stats *obs_data);

#endif