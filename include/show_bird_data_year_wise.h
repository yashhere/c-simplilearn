#ifndef SHOW_BIRD_DATA_YEAR_WISE_H
#define SHOW_BIRD_DATA_YEAR_WISE_H

#include "main.h"
#include "bird.h"
#include "bird_entry.h"
#include "menu.h"
#include "list_birds_in_a_year.h"

void show_bird_seen_in_all_years(birds birds_data, stats obs_data);

#endif