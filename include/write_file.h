#ifndef WRITE_FILE_H
#define WRITE_FILE_H

#include "main.h"
#include "bird_entry.h"
#include "bird.h"

void write_data(const char FILE_NAME[], void *list, size_t size_of_list);
void write_data_in_bird(const char FILE_NAME[], bird *data, size_t size_of_list);
void write_data_in_observation(const char FILE_NAME[], bird_entry *data, size_t size_of_list);

#endif