#ifndef MENU_H
#define MENU_H

#include "main.h"
#include "bird.h"
#include "bird_entry.h"
#include "remove_non_common.h"
#include "write_file.h"

void print_options();
void show_choices(birds data, stats *data1);
void filter_birds_by_id(birds *data, int id, birds *filtered_id);
void filter_birds_by_keyword(birds *data, char *keyword, birds *filtered_id);
void filter_birds_by_year_range(stats *data, int lower_year, int higher_year, stats *filtered_birds);
void filter_birds_by_year_and_month(stats *data, int lower_year, int higher_year, int lower_month, int higher_month, stats *filtered_birds);
int filter_birds_by_id_and_year_and_month(stats *data, int lower_year, int higher_year, int lower_month, int higher_month, int id);
int search_entry(stats *data, bird_entry record);
int compare_bird_entries(bird_entry a, bird_entry b);
void search_entry_in_stats(stats *data, int id, stats *filtered_birds);

#endif