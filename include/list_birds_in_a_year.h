#ifndef LIST_BIRDS_IN_A_YEAR_H
#define LIST_BIRDS_IN_A_YEAR_H

#include "menu.h"
int get_bird_name_by_id(birds birds_data, int id);
void show_birds_seen_in_a_year(birds birds_data, stats obs_data);

#endif