#ifndef SHOW_BIRD_DATA_MONTH_WISE_H
#define SHOW_BIRD_DATA_MONTH_WISE_H

#include "main.h"
#include "bird.h"
#include "bird_entry.h"
#include "menu.h"
#include "list_birds_in_a_year.h"

void show_birds_month_wise(birds bird_data, stats obs_data);

#endif