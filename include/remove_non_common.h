#ifndef REMOVE_NON_COMMON_H
#define REMOVE_NON_COMMON_H

#include "main.h"
#include "bird.h"
#include "bird_entry.h"

void remove_element_from_observation(stats **entries, int index);
int search(int id, bird *birds, int birds_size);
void filter_entries(birds *bird_data, stats *entries);

#endif