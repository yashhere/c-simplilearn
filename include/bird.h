#ifndef BIRD_INTERFACE_H
#define BIRD_INTERFACE_H

#include "main.h"

typedef struct
{
    uint16_t id;
    uint8_t len;
    char name[256];
} bird;

typedef struct
{
    unsigned size;
    bird *birds;
} birds;

void print_bird(bird *bird_data, int size);
int compare_bird_id(const void *a, const void *b);
void read_bird_file(birds *data);

#endif