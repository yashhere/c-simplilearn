// #include "main.h"
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

typedef struct
{
    uint16_t id;
    uint8_t len;
    char name[256];
} bird;

int main(int argc, char const *argv[])
{
    FILE *file;
    const char FILE_NAME[] = "birds.bin";
    file = fopen(FILE_NAME, "wb");
    if (file == NULL)
    {
        perror(FILE_NAME);
        return (-1);
    }

    bird list[] = {
        {1, 6, "heron"},
        {2, 7, "parrot"},
        {3, 8, "swallow"},
        {4, 5, "kiwi"},
        {5, 4, "owl"},
        {6, 6, "goose"},
        // {7, 5, "gulls"},
        {8, 5, "lark"},
        {9, 10, "cormorant"},
        {10, 10, "sandpiper"},
        {11, 10, "blackbird"},
        {12, 7, "pigeon"}};

    fwrite(list, sizeof(list), 1, file);
    fclose(file);

    // int i = 0;
    // while (i < 11)
    // {
    //     uint16_t id;
    //     uint8_t len;
    //     char *name;

    //     int id_temp, len_temp;
    //     scanf("%d %d", &id_temp, &len_temp);

    //     id = (uint16_t)id_temp;
    //     len = (uint8_t)len_temp;

    //     name = (char *)malloc(sizeof(char) * len);
    //     if (!name)
    //     {
    //         fprintf(stderr, "Couldn't allocate %lu bytes of data to name!\n", sizeof(char) * len);
    //         return 1;
    //     }
    //     scanf("%s", name);

    //     bird *a = (bird *)malloc(sizeof(bird));
    //     if (!a)
    //     {
    //         fprintf(stderr, "Couldn't allocate %lu bytes of data to bird 'a'!\n", sizeof(bird));
    //         return 1;
    //     }
    //     a->id = (uint16_t)id_temp;
    //     a->len = (uint8_t)len_temp;
    //     a->name = (char *)malloc(sizeof(char) * a->len);
    //     strcpy(a->name, name);

    //     fwrite(a, sizeof(bird), 1, file);
    //     free(name);
    //     free(a);
    //     i++;
    // }
    // fclose(file);

    // bird *myStruct;
    // myStruct = (bird *)malloc(sizeof(bird));
    // fread(&myStruct, sizeof(bird), 1, file);

    // printf("%d\n", myStruct->id);
    // // printf("%d\n", myStruct.len);
    // // myStruct.name = malloc(myStruct.len + 1);
    // // printf("%s\n", myStruct.name);

    return 0;
}
