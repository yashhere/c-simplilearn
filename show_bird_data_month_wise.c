#include "include/show_bird_data_month_wise.h"

void show_birds_month_wise(birds bird_data, stats obs_data)
{
    printf("Please enter bird id: ");
    int id;
    scanf("%d", &id);

    int start_year, end_year;
    char string[100];
    printf("Please input a start year[1998]: ");
    memset(string, '\0', sizeof string);
    getchar();
    fgets(string, sizeof string, stdin);
    string[strlen(string) - 1] = '\0';
    if (strlen(string) == 0)
    {
        strcpy(string, "1998");
    }
    start_year = atoi(string);

    printf("Please input end year[2018]: ");
    memset(string, '\0', sizeof string);
    fgets(string, sizeof string, stdin);
    string[strlen(string) - 1] = '\0';
    if (strlen(string) == 0)
    {
        strcpy(string, "2018");
    }
    end_year = atoi(string);

    int month;
    printf("Please input month: ");
    memset(string, '\0', sizeof string);
    fgets(string, sizeof string, stdin);
    string[strlen(string) - 1] = '\0';
    if (strlen(string) == 0)
    {
        printf("Month is a required field.\n");
        return;
    }
    month = atoi(string);

    stats filtered_birds = {0, NULL};
    filter_birds_by_year_and_month(&obs_data, start_year, end_year, month, month, &filtered_birds);
    if (filtered_birds.size == 0)
    {
        printf("No birds in given years found.\n");
    }
    else
    {
        int bird_index = get_bird_name_by_id(bird_data, id);
        if (bird_index == -1)
        {
            perror("Bird does not exist in our records.");
        }
        else
        {
            printf("\n*****************************************************************\n");
            printf("BIRD NAME = %s, BIRD ID = %d\n", bird_data.birds[bird_index].name, id);
            printf("*****************************************************************\n");
            for (int j = start_year; j <= end_year; j++)
            {
                int count = 0;
                printf("\nYEAR %d:\n", j);
                for (int i = 0; i < filtered_birds.size; i++)
                {
                    if (id == filtered_birds.data[i].id && j == filtered_birds.data[i].year)
                    {
                        count++;
                        printf("id = %d, month = %d, year = %d, number observed = %d\n", filtered_birds.data[i].id, filtered_birds.data[i].month, filtered_birds.data[i].year, filtered_birds.data[i].number_observed);
                    }
                }

                if (count == 0)
                {
                    printf("No bird record found matching the given criteria.\n");
                }
            }
        }
    }
    free(filtered_birds.data);
}