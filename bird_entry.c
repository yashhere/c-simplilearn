#include "include/bird_entry.h"

int compare_id(const void *a, const void *b)
{

    bird_entry *A = (bird_entry *)a;
    bird_entry *B = (bird_entry *)b;

    return (A->id - B->id);
}

void read_observation_file(stats *data)
{
    FILE *observation_file;
    const char OBS_FILE_NAME[] = "observations.bin";
    observation_file = fopen(OBS_FILE_NAME, "rb");
    if (observation_file == NULL)
    {
        perror(OBS_FILE_NAME);
        exit(EXIT_FAILURE);
    }

    int index = 0;
    int size = 0;
    size = size + 1;
    bird_entry *bird_data = (bird_entry *)malloc(sizeof(bird_entry) * size);
    bird_entry def_values = {0};

    *bird_data = def_values;
    while (1)
    {
        bird_entry temp;
        fread(&temp, sizeof(bird_entry), 1, observation_file);
        if (feof(observation_file))
        {
            break;
        }

        bird_data[index++] = temp;

        size++;
        bird_entry *tmp = realloc(bird_data, sizeof(bird_entry) * size);

        if (tmp == NULL)
        {
            free(tmp);
            perror("Reallocation failed\n");
            exit(EXIT_FAILURE);
        }
        else if (bird_data != tmp)
        {
            bird_data = tmp;
        }
        tmp = NULL;
    }

    fclose(observation_file);
    qsort(bird_data, index, sizeof(bird_entry), compare_id);

    data->size = index;
    data->data = bird_data;
}

void print_stats(bird_entry *bird_data, int size)
{
    printf("\n*********************************************************\n");
    for (int i = 0; i < size; i++)
    {
        printf("year=%d, month=%d, day=%d id=%d number=%d\n", bird_data[i].year, bird_data[i].month, bird_data[i].day, bird_data[i].id, bird_data[i].number_observed);
    }
    printf("\n*********************************************************\n");
}
