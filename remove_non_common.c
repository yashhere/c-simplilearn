#include "include/remove_non_common.h"
#include "include/write_file.h"

void remove_element_from_observation(stats **entries, int index)
{
    bird_entry *data = (*entries)->data;

    // TODO check if observations.bin file has only one entry. In that case, the following statement will fail.
    bird_entry *temp = (bird_entry *)malloc(((*entries)->size - 1) * sizeof(bird_entry));
    bird_entry def_values = {0};

    *temp = def_values;

    int k = 0;
    for (int i = 0; i < (*entries)->size; ++i)
    {
        if (i != index)
        {
            temp[k++] = data[i];
        }
    }

    free((*entries)->data);
    (*entries)->data = NULL;

    (*entries)->data = temp;
    (*entries)->size -= 1;

    temp = NULL;
}

int search(int id, bird *birds, int birds_size)
{
    int i = 0, j = birds_size - 1;
    while (i <= j)
    {
        int m = i + (j - i) / 2;
        if (id > birds[m].id)
        {
            i = m + 1;
        }
        else if (id < birds[m].id)
        {
            j = m - 1;
        }
        else
        {
            return 1;
        }
    }
    return 0;
}

void filter_entries(birds *bird_data, stats *entries)
{
    bird *birds = bird_data->birds;
    size_t birds_size = bird_data->size;

    int i = 0, flag = 0;

    for (i = 0; i < entries->size; i++)
    {
        if (flag == 1 && i != 0 && entries->data[i - 1].id == entries->data[i].id)
        {
            // printf("Similar entries found id = %d\n", entries->data[i].id);
            continue;
        }

        if (search(entries->data[i].id, birds, birds_size) == 1)
        {
            // printf("entry found id = %d\n", entries->data[i].id);
            flag = 1;
        }
        else
        {
            // printf("Removing id = %d\n", entries->data[i].id);
            flag = 0;
            remove_element_from_observation(&entries, i);
            i--;
        }
    }
    write_data_in_observation("observations.bin", entries->data, entries->size);
}