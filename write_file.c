#include "include/write_file.h"

void write_data_in_bird(const char FILE_NAME[], bird *data, size_t size_of_list)
{
    FILE *file;
    file = fopen(FILE_NAME, "wb");

    for (int i = 0; i < size_of_list; i++)
    {
        fwrite(&data[i], sizeof(bird), 1, file);
    }
    fclose(file);
}

void write_data_in_observation(const char FILE_NAME[], bird_entry *data, size_t size_of_list)
{
    FILE *file;
    file = fopen(FILE_NAME, "wb");

    for (int i = 0; i < size_of_list; i++)
    {
        fwrite(&data[i], sizeof(bird_entry), 1, file);
    }
    fclose(file);
}