#include "include/show_bird_data_year_wise.h"

void show_bird_seen_in_all_years(birds birds_data, stats obs_data)
{
    printf("Please enter bird id: ");
    int id;
    scanf("%d", &id);

    int start_year, end_year;
    char string[100];
    printf("Please input a start year[1998]: ");
    memset(string, '\0', sizeof string);
    getchar();
    fgets(string, sizeof string, stdin);
    string[strlen(string) - 1] = '\0';
    if (strlen(string) == 0)
    {
        strcpy(string, "1998");
    }
    start_year = atoi(string);

    printf("Please input end year[2018]: ");
    memset(string, '\0', sizeof string);
    fgets(string, sizeof string, stdin);
    string[strlen(string) - 1] = '\0';
    if (strlen(string) == 0)
    {
        strcpy(string, "2018");
    }
    end_year = atoi(string);

    stats filtered_birds = {0, NULL};
    filter_birds_by_year_range(&obs_data, start_year, end_year, &filtered_birds);
    if (filtered_birds.size == 0)
    {
        printf("No birds in given years found.\n");
    }
    else
    {
        int bird_index = get_bird_name_by_id(birds_data, id);
        if (bird_index == -1)
        {
            perror("Bird does not exist in our records.");
        }
        else
        {
            int count = 0;
            printf("\n*****************************************************************\n");
            printf("BIRD NAME = %s, BIRD ID = %d\n", birds_data.birds[bird_index].name, id);
            printf("*****************************************************************\n");
            for (int i = 0; i < filtered_birds.size; i++)
            {
                if (id == filtered_birds.data[i].id)
                {
                    count++;
                    printf("year = %d, number observed = %d\n", filtered_birds.data[i].year, filtered_birds.data[i].number_observed);
                }
            }
            if (count == 0)
            {
                printf("No bird record found matching the given id.\n");
            }
        }
    }
    free(filtered_birds.data);
}