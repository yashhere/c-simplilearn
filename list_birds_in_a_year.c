#include "include/list_birds_in_a_year.h"

int get_bird_name_by_id(birds birds_data, int id)
{
    for (int i = 0; i < birds_data.size; i++)
    {
        if (birds_data.birds[i].id == id)
        {
            return i;
        }
    }
    return -1;
}

void show_birds_seen_in_a_year(birds birds_data, stats obs_data)
{
    printf("Please enter a year: ");
    int year;
    scanf("%d", &year);
    stats filtered_birds = {0, NULL};
    filter_birds_by_year_range(&obs_data, year, year, &filtered_birds);
    if (filtered_birds.size == 0)
    {
        printf("No birds found in given year.\n");
    }
    else
    {
        printf("Birds seen in the year %d are:\n", year);
        for (int i = 0; i < filtered_birds.size; i++)
        {
            int bird_index = get_bird_name_by_id(birds_data, filtered_birds.data[i].id);
            if (bird_index == -1)
            {
                perror("Bird does not exist in our records.");
            }
            else
            {
                printf("name= %s, id = %d, number observed = %d\n", birds_data.birds[bird_index].name, filtered_birds.data[i].id, filtered_birds.data[i].number_observed);
            }
        }
    }
    free(filtered_birds.data);
}