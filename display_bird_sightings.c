#include "include/display_bird_sightings.h"

int compare_sightings(const void *a, const void *b)
{

    bird_entry *A = (bird_entry *)a;
    bird_entry *B = (bird_entry *)b;

    return (A->number_observed - B->number_observed);
}

void filter_observations_by_id(stats *obs_data, int id, stats *filtered_birds)
{
    int i = 0;
    size_t size = obs_data->size;
    bird_entry *records = obs_data->data;

    int k = 0;
    for (i = 0; i < size; i++)
    {
        if (records[i].id == id)
        {
            if (filtered_birds->data == NULL)
            {
                filtered_birds->data = (bird_entry *)malloc(sizeof(bird_entry) * 1);
                filtered_birds->data[k++] = records[i];
                filtered_birds->size++;
            }
            else
            {
                // http://www.c-faq.com/malloc/realloc.html
                bird_entry *tmp = realloc(filtered_birds->data, sizeof(bird_entry) * (k + 1));

                if (tmp == NULL)
                {
                    free(tmp);
                    perror("Reallocation failed\n");
                    exit(EXIT_FAILURE);
                }
                else if (tmp == filtered_birds->data)
                {
                    tmp = NULL;
                }
                else
                {
                    filtered_birds->data = tmp;
                    filtered_birds->data[k++] = records[i];
                    filtered_birds->size++;
                    tmp = NULL;
                }
            }
        }
    }
}

void display_bird_sightings(birds *bird_data, stats *obs_data)
{
    char string[100];
    printf("Please input a search string: ");
    memset(string, '\0', sizeof string);
    fgets(string, sizeof(string), stdin);
    string[strlen(string) - 1] = '\0';

    birds filtered_ids = {0, NULL};
    filter_birds_by_keyword(bird_data, string, &filtered_ids);
    if (filtered_ids.size == 0)
    {
        printf("No bird name matching given keyword found.\n");
    }
    else
    {
        for (int i = 0; i < filtered_ids.size; i++)
        {
            stats filtered_birds = {0, NULL};
            filter_observations_by_id(obs_data, filtered_ids.birds[i].id, &filtered_birds);
            qsort(filtered_birds.data, filtered_birds.size, sizeof(bird_entry), compare_sightings);

            printf("\n***************************************************************\n");
            printf("Statistics for bird %s.\n", filtered_ids.birds[i].name);
            printf("***************************************************************\n");
            if (filtered_birds.size == 0)
            {
                printf("No records found for the bird.\n");
            }
            else
            {
                for (int j = 0; j < filtered_birds.size; j++)
                {
                    printf("year = %d, number observed = %d\n", filtered_birds.data[j].year, filtered_birds.data[j].number_observed);
                }
            }
            printf("\n");
            free(filtered_birds.data);
            filtered_birds.data = NULL;
        }
    }
    free(filtered_ids.birds);
    filtered_ids.birds = NULL;
}