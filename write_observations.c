// #include "main.h"
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

typedef struct
{
    int reserved : 12;
    unsigned int year : 11;
    unsigned int month : 4;
    unsigned int day : 5;
    short id;
    short number_observed;
} bird_entry;

int main(int argc, char const *argv[])
{
    FILE *file;
    const char FILE_NAME[] = "observations.bin";
    file = fopen(FILE_NAME, "wb");
    if (file == NULL)
    {
        perror(FILE_NAME);
        return (-1);
    }

    bird_entry list[] = {
        {0 << 12, 2016, 2, 25, 13, 53},
        {0 << 12, 1998, 6, 23, 1, 45},
        {0 << 12, 2007, 1, 2, 2, 200},
        {0 << 12, 2013, 7, 13, 3, 197},
        {0 << 12, 2004, 4, 20, 4, 562},
        {0 << 12, 2015, 3, 7, 5, 1032},
        {0 << 12, 2008, 1, 12, 15, 1},
        {0 << 12, 2006, 5, 10, 6, 12},
        {0 << 12, 2015, 2, 4, 0, 4},
        {0 << 12, 2017, 5, 15, 7, 232},
        {0 << 12, 2013, 1, 20, 7, 414},
        {0 << 12, 2018, 3, 1, 7, 19},
        {0 << 12, 2008, 4, 11, 8, 536},
        {0 << 12, 2010, 9, 12, 10, 54},
        {0 << 12, 2011, 12, 14, 11, 69},
        {0 << 12, 2011, 2, 24, 11, 69},
        {0 << 12, 2011, 4, 17, 11, 69},
        {0 << 12, 2016, 1, 20, 6, 12},
        {0 << 12, 1998, 11, 19, 6, 12},
        {0 << 12, 2016, 1, 29, 3, 125},
        {0 << 12, 2016, 4, 30, 10, 52}

    };

    fwrite(list, sizeof(list), 1, file);
    fclose(file);

    // int i = 0;
    // while (i < 11)
    // {
    //     int id;
    //     uint8_t len;
    //     char *name;

    //     int id_temp, len_temp;
    //     scanf("%d %d", &id_temp, &len_temp);

    //     id = (int)id_temp;
    //     len = (uint8_t)len_temp;

    //     name = (char *)malloc(sizeof(char) * len);
    //     if (!name)
    //     {
    //         fprintf(stderr, "Couldn't allocate %lu bytes of data to name!\n", sizeof(char) * len);
    //         return 1;
    //     }
    //     scanf("%s", name);

    //     bird *a = (bird *)malloc(sizeof(bird));
    //     if (!a)
    //     {
    //         fprintf(stderr, "Couldn't allocate %lu bytes of data to bird 'a'!\n", sizeof(bird));
    //         return 1;
    //     }
    //     a->id = (int)id_temp;
    //     a->len = (uint8_t)len_temp;
    //     a->name = (char *)malloc(sizeof(char) * a->len);
    //     strcpy(a->name, name);

    //     fwrite(a, sizeof(bird), 1, file);
    //     free(name);
    //     free(a);
    //     i++;
    // }
    // fclose(file);

    // bird *myStruct;
    // myStruct = (bird *)malloc(sizeof(bird));
    // fread(&myStruct, sizeof(bird), 1, file);

    // printf("%d\n", myStruct->id);
    // // printf("%d\n", myStruct.len);
    // // myStruct.name = malloc(myStruct.len + 1);
    // // printf("%s\n", myStruct.name);

    return 0;
}
