CC=gcc
DEP_HEADERS=main.h bird_entry.h bird.h remove_non_common.h write_file.h menu.h list_birds_in_a_year.h show_bird_data_year_wise.h show_bird_data_month_wise.h delete_bird_observations.h display_bird_sightings.h
MKDIR_P=mkdir -p

ODIR=./obj
BINDIR=./bin

IDIR=./include
LDIR=./lib

# DEP_HEADERS := $(shell find $(IDIR) -name '*.h -type f -printf "%f\n"')
CFLAGS=-I$(IDIR) -ggdb3 -Wall -Werror -fsanitize=address -fstack-protector -Wl,-z,relro -Wl,-z,now -Wformat-security -Wpointer-arith -Wformat-nonliteral -Winit-self -Wl,-O1
LFLAGS=-L$(LDIR)

LIBS=-lm

# SFILES = main.c read_birds.c read_observations.c
OFILES = main.o bird.o bird_entry.o remove_non_common.o write_file.o menu.o list_birds_in_a_year.o show_bird_data_year_wise.o show_bird_data_month_wise.o delete_bird_observations.o display_bird_sightings.o
# OFILES = $(DEP_HEADERS:.h=.o)
OBJ = $(patsubst %,$(ODIR)/%,$(OFILES))
DEPS = $(patsubst %,$(IDIR)/%,$(DEP_HEADERS))

# rule to execute all prerequisites
all: directory main run

directory:
	$(MKDIR_P) $(BINDIR) $(ODIR)

# rule to generate .o files from .c file
$(ODIR)/%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

# rule for generating exeutable
main: $(OBJ)
	$(CC) -o $(BINDIR)/$@ $^ $(CFLAGS) $(LFLAGS) $(LIBS)

run: main
	./bin/main

# remove .o and demo executable
clean: 
	rm $(ODIR)/*.o $(BINDIR)/main

# remove obj and bin directories
cleanall: 
	rm -rf $(ODIR) $(BINDIR)
