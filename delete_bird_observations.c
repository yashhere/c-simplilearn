#include "include/delete_bird_observations.h"

void delete_bird_observations(birds bird_data, stats *obs_data)
{
    printf("Please enter bird id: ");
    int id;
    scanf("%d", &id);

    int start_year, end_year;
    char string[100];
    printf("Please input a start year[1998]: ");
    memset(string, '\0', sizeof string);
    getchar();
    fgets(string, sizeof string, stdin);
    string[strlen(string) - 1] = '\0';
    if (strlen(string) == 0)
    {
        strcpy(string, "1998");
    }
    start_year = atoi(string);

    printf("Please input end year[2018]: ");
    memset(string, '\0', sizeof string);
    fgets(string, sizeof string, stdin);
    string[strlen(string) - 1] = '\0';
    if (strlen(string) == 0)
    {
        strcpy(string, "2018");
    }
    end_year = atoi(string);

    int start_month, end_month;
    printf("Please input start month[1]: ");
    memset(string, '\0', sizeof string);
    fgets(string, sizeof string, stdin);
    string[strlen(string) - 1] = '\0';
    if (strlen(string) == 0)
    {
        strcpy(string, "1");
    }
    start_month = atoi(string);

    printf("Please input end month[12]: ");
    memset(string, '\0', sizeof string);
    fgets(string, sizeof string, stdin);
    string[strlen(string) - 1] = '\0';
    if (strlen(string) == 0)
    {
        strcpy(string, "12");
    }
    end_month = atoi(string);

    int count = 0;
    if ((count = filter_birds_by_id_and_year_and_month(obs_data, start_year, end_year, start_month, end_month, id)) == -1)
    {
        printf("No bird record could be found matching given information.\n");
    }
    else
    {
        write_data_in_observation("observations.bin", obs_data->data, obs_data->size);
        printf("Deleted %d records from observations.bin file.\n", count);
    }
}