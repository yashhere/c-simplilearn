#include "include/menu.h"

void print_options()
{
    printf("\n\n");
    printf("1. Show Bird - List Bird Name identified by user supplied bird ID\n");
    printf("2. Filter birds by keyword - List all birds for the supplied keyword appearing in bird name\n");
    printf("3. Sightings by year - Filter birds by supplied year\n");
    printf("4. Show Stats by year - Show bird stats by supplied year range\n");
    printf("5. Show Year/Month Stats - Show bird stats by supplied year range and month\n");
    printf("6. Delete Observations - Delete bird observations reported in the supplied bird id, year and month range\n");
    printf("7. Stats by bird keyword - Search bird by supplied keyword and list the statistics one below the other\n");
    printf("8. Exit\n");
    printf("\n\n");
}

void show_choices(birds data, stats *data1)
{
    int choice = -1;

    do
    {
        print_options();
        printf("Select an option: ");
        scanf("%d", &choice);
        birds filtered_ids = {0, NULL};
        stats filtered_birds = {0, NULL};
        char string[100];

        int years[2], month[2], k = 0, id, year;

        char *pt;
        switch (choice)
        {
        case 1:
            printf("Please input a bird ID: ");
            scanf("%d", &id);
            filter_birds_by_id(&data, id, &filtered_ids);

            if (filtered_ids.size == 0)
            {
                printf("No bird id matching given id found.\n");
            }
            else
            {
                for (int i = 0; i < filtered_ids.size; i++)
                {
                    printf("id = %d, name = %s\n", filtered_ids.birds[i].id, filtered_ids.birds[i].name);
                }
            }
            free(filtered_ids.birds);
            break;
        case 2:
            printf("Please input a search string: ");
            memset(string, '\0', sizeof string);
            getchar();
            fgets(string, sizeof(string), stdin);
            string[strlen(string) - 1] = '\0';
            filter_birds_by_keyword(&data, string, &filtered_ids);
            if (filtered_ids.size == 0)
            {
                printf("No bird name matching given keyword found.\n");
            }
            else
            {
                for (int i = 0; i < filtered_ids.size; i++)
                {
                    printf("id = %d, name = %s\n", filtered_ids.birds[i].id, filtered_ids.birds[i].name);
                }
            }
            free(filtered_ids.birds);
            break;
        case 3:
            printf("Please input a year: ");
            scanf("%d", &year);

            filter_birds_by_year_range(data1, year, year, &filtered_birds);
            if (filtered_birds.size == 0)
            {
                printf("No birds in given year found.\n");
            }
            else
            {
                for (int i = 0; i < filtered_birds.size; i++)
                {
                    printf("id = %d, number observed = %d\n", filtered_birds.data[i].id, filtered_birds.data[i].number_observed);
                }
            }
            free(filtered_birds.data);
            break;
        case 4:
            printf("Please input a year range in yyyy-yyyy format: ");
            memset(string, '\0', sizeof string);
            getchar();
            fgets(string, sizeof string, stdin);
            string[strlen(string) - 1] = '\0';

            k = 0;
            memset(years, 0, sizeof years);
            pt = NULL;
            pt = strtok(string, "-");
            while (pt != NULL)
            {
                years[k++] = atoi(pt);
                pt = strtok(NULL, "-");
            }

            filter_birds_by_year_range(data1, years[0], years[1], &filtered_birds);
            if (filtered_birds.size == 0)
            {
                printf("No birds in given years found.\n");
            }
            else
            {
                for (int i = 0; i < filtered_birds.size; i++)
                {
                    printf("id = %d, year = %d, number observed = %d\n", filtered_birds.data[i].id, filtered_birds.data[i].year, filtered_birds.data[i].number_observed);
                }
            }
            free(filtered_birds.data);
            break;
        case 5:
            printf("Please input a year range in yyyy-yyyy format: ");
            memset(string, '\0', sizeof string);
            getchar();
            fgets(string, sizeof string, stdin);
            string[strlen(string) - 1] = '\0';

            pt = NULL;
            k = 0;
            memset(years, 0, sizeof years);
            pt = strtok(string, "-");
            while (pt != NULL)
            {
                years[k++] = atoi(pt);
                pt = strtok(NULL, "-");
            }

            memset(string, '\0', sizeof string);
            printf("Please input a month range in mm-mm format: ");
            fgets(string, sizeof string, stdin);

            pt = NULL;
            k = 0;
            memset(month, 0, sizeof month);

            pt = strtok(string, "-");
            while (pt != NULL)
            {
                month[k++] = atoi(pt);
                pt = strtok(NULL, "-");
            }

            filter_birds_by_year_and_month(data1, years[0], years[1], month[0], month[1], &filtered_birds);
            if (filtered_birds.size == 0)
            {
                printf("No birds in given years found.\n");
            }
            else
            {
                for (int i = 0; i < filtered_birds.size; i++)
                {
                    printf("id = %d, year = %d, number observed = %d\n", filtered_birds.data[i].id, filtered_birds.data[i].year, filtered_birds.data[i].number_observed);
                }
            }
            free(filtered_birds.data);
            break;

        case 6:
            printf("Please enter bird id: ");
            scanf("%d", &id);

            printf("Please input a year range in yyyy-yyyy format: ");
            memset(string, '\0', sizeof string);
            getchar();
            fgets(string, sizeof string, stdin);
            string[strlen(string) - 1] = '\0';

            pt = NULL;
            k = 0;
            memset(years, 0, sizeof years);
            pt = strtok(string, "-");
            while (pt != NULL)
            {
                years[k++] = atoi(pt);
                pt = strtok(NULL, "-");
            }

            memset(string, '\0', sizeof string);
            printf("Please input a month range in mm-mm format: ");
            fgets(string, sizeof string, stdin);

            pt = NULL;
            k = 0;
            memset(month, 0, sizeof month);

            pt = strtok(string, "-");
            while (pt != NULL)
            {
                month[k++] = atoi(pt);
                pt = strtok(NULL, "-");
            }

            int count = 0;
            if ((count = filter_birds_by_id_and_year_and_month(data1, years[0], years[1], month[0], month[1], id)) == -1)
            {
                printf("No bird record could be found matching given information.\n");
            }
            else
            {
                write_data_in_observation("observations.bin", data1->data, data1->size);
                printf("Deleted %d records from observations.bin file.\n", count);
            }
            break;
        case 7:
            printf("Please input a search string: ");
            memset(string, '\0', sizeof string);
            getchar();
            fgets(string, sizeof(string), stdin);
            string[strlen(string) - 1] = '\0';
            filter_birds_by_keyword(&data, string, &filtered_ids);
            if (filtered_ids.size == 0)
            {
                printf("No bird name matching given keyword found.\n");
            }
            else
            {
                for (int i = 0; i < filtered_ids.size; i++)
                {
                    filtered_birds.size = 0;
                    filtered_birds.data = NULL;
                    search_entry_in_stats(data1, filtered_ids.birds[i].id, &filtered_birds);
                    if (filtered_birds.size == 0)
                    {
                        printf("No record belonging to bird %s found in the observations file.\n", filtered_ids.birds[i].name);
                    }
                    else
                    {
                        printf("**********************************************************************************************************************\n");
                        for (int j = 0; j < filtered_birds.size; j++)
                        {
                            printf("Name: %s, id=%d, year=%d, month=%d, day=%d, number_observed=%d\n", filtered_ids.birds[i].name, filtered_ids.birds[i].id, filtered_birds.data[j].year, filtered_birds.data[j].month, filtered_birds.data[j].day, filtered_birds.data[j].number_observed);
                        }
                        printf("**********************************************************************************************************************\n\n");
                    }
                    free(filtered_birds.data);
                }
            }
            free(filtered_ids.birds);
            break;
        case 8:
            printf("Exiting...\n");
            break;
        }
    } while (choice != 8);
}

void filter_birds_by_id(birds *data, int id, birds *filtered_id)
{
    int i = 0;
    size_t size = data->size;
    bird *records = data->birds;

    int k = 0;
    for (i = 0; i < size; i++)
    {
        if (records[i].id == id)
        {
            if (filtered_id->birds == NULL)
            {
                filtered_id->birds = (bird *)malloc(sizeof(bird) * 1);
                filtered_id->birds[k++] = records[i];
                filtered_id->size++;
            }
            else
            {
                // http://www.c-faq.com/malloc/realloc.html
                bird *tmp = realloc(filtered_id->birds, sizeof(bird) * (k + 1));

                if (tmp == NULL)
                {
                    free(tmp);
                    perror("Reallocation failed\n");
                    exit(EXIT_FAILURE);
                }
                else if (tmp == filtered_id->birds)
                {
                    tmp = NULL;
                }
                else
                {
                    filtered_id->birds = tmp;
                    filtered_id->birds[k++] = records[i];
                    filtered_id->size++;
                    tmp = NULL;
                }
            }
        }
    }
}

void filter_birds_by_keyword(birds *data, char *keyword, birds *filtered_id)
{
    int i = 0;
    size_t size = data->size;
    bird *records = data->birds;

    int k = 0;
    for (i = 0; i < size; i++)
    {
        if (strstr(records[i].name, keyword) != NULL)
        {
            if (filtered_id->birds == NULL)
            {
                filtered_id->birds = (bird *)malloc(sizeof(bird) * 1);
                filtered_id->birds[k++] = records[i];
                filtered_id->size++;
            }
            else
            {
                bird *tmp = realloc(filtered_id->birds, sizeof(bird) * (k + 1));

                if (tmp == NULL)
                {
                    free(tmp);
                    perror("Reallocation failed\n");
                    exit(EXIT_FAILURE);
                }
                else if (tmp == filtered_id->birds)
                {
                    tmp = NULL;
                }
                else
                {
                    filtered_id->birds = tmp;
                    filtered_id->birds[k++] = records[i];
                    filtered_id->size++;
                    tmp = NULL;
                }
            }
        }
    }
}

void filter_birds_by_year_range(stats *data, int lower_year, int higher_year, stats *filtered_birds)
{
    int i = 0;
    size_t size = data->size;
    bird_entry *records = data->data;

    int k = 0;
    for (i = 0; i < size; i++)
    {
        if (records[i].year >= lower_year && records[i].year <= higher_year)
        {
            if (filtered_birds->data == NULL)
            {
                filtered_birds->data = (bird_entry *)malloc(sizeof(bird_entry) * 1);
                filtered_birds->data[k++] = records[i];
                filtered_birds->size++;
            }
            else
            {
                bird_entry *tmp = realloc(filtered_birds->data, sizeof(bird_entry) * (k + 1));

                if (tmp == NULL)
                {
                    free(tmp);
                    perror("Reallocation failed\n");
                    exit(EXIT_FAILURE);
                }
                else if (tmp == filtered_birds->data)
                {
                    tmp = NULL;
                }
                else
                {
                    filtered_birds->data = tmp;
                    filtered_birds->data[k++] = records[i];
                    filtered_birds->size++;
                    tmp = NULL;
                }
            }
        }
    }
}

void filter_birds_by_year_and_month(stats *data, int lower_year, int higher_year, int lower_month, int higher_month, stats *filtered_birds)
{
    int i = 0;
    size_t size = data->size;
    bird_entry *records = data->data;

    int k = 0;
    for (i = 0; i < size; i++)
    {
        if (records[i].year >= lower_year && records[i].year <= higher_year)
        {
            if (records[i].month >= lower_month && records[i].month <= higher_month)
            {

                if (filtered_birds->data == NULL)
                {
                    filtered_birds->data = (bird_entry *)malloc(sizeof(bird_entry) * 1);
                    filtered_birds->data[k++] = records[i];
                    filtered_birds->size++;
                }
                else
                {
                    bird_entry *tmp = realloc(filtered_birds->data, sizeof(bird_entry) * (k + 1));

                    if (tmp == NULL)
                    {
                        free(tmp);
                        perror("Reallocation failed\n");
                        exit(EXIT_FAILURE);
                    }
                    else if (tmp == filtered_birds->data)
                    {
                        tmp = NULL;
                    }
                    else
                    {
                        filtered_birds->data = tmp;
                        filtered_birds->data[k++] = records[i];
                        filtered_birds->size++;
                        tmp = NULL;
                    }
                }
            }
        }
    }
}

int compare_bird_entries(bird_entry a, bird_entry b)
{
    if (a.id == b.id && a.year == b.year && a.month == b.month && a.number_observed == b.number_observed && a.day == b.day)
    {
        return 1;
    }
    return 0;
}
int search_entry(stats *data, bird_entry record)
{
    for (int i = 0; i < data->size; i++)
    {
        if (compare_bird_entries(data->data[i], record) == 1)
        {
            return i;
        }
    }
    return -1;
}

int filter_birds_by_id_and_year_and_month(stats *data, int lower_year, int higher_year, int lower_month, int higher_month, int id)
{
    stats filtered_birds = {0, NULL};
    filter_birds_by_year_and_month(data, lower_year, higher_year, lower_month, higher_month, &filtered_birds);

    int count = 0;

    if (filtered_birds.size == 0)
    {
        return -1;
    }

    for (int i = 0; i < filtered_birds.size; i++)
    {
        if (filtered_birds.data[i].id == id)
        {
            int index = search_entry(data, filtered_birds.data[i]);
            if (index != -1)
            {
                count++;
                remove_element_from_observation(&data, index);
            }
        }
    }

    free(filtered_birds.data);
    return count;
}

void search_entry_in_stats(stats *data, int id, stats *filtered_birds)
{
    int i = 0;
    size_t size = data->size;
    bird_entry *records = data->data;

    int k = 0;
    for (i = 0; i < size; i++)
    {
        if (records[i].id == id)
        {
            if (filtered_birds->data == NULL)
            {
                filtered_birds->data = (bird_entry *)malloc(sizeof(bird_entry) * 1);
                filtered_birds->data[k++] = records[i];
                filtered_birds->size++;
            }
            else
            {
                bird_entry *tmp = realloc(filtered_birds->data, sizeof(bird_entry) * (k + 1));

                if (tmp == NULL)
                {
                    free(tmp);
                    perror("Reallocation failed\n");
                    exit(EXIT_FAILURE);
                }
                else if (tmp == filtered_birds->data)
                {
                    tmp = NULL;
                }
                else
                {
                    filtered_birds->data = tmp;
                    filtered_birds->data[k++] = records[i];
                    filtered_birds->size++;
                    tmp = NULL;
                }
            }
        }
    }
}