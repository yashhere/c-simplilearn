#include "include/bird_interface.h"

void print_bird(bird *bird_data, int size)
{
    for (int i = 0; i < size; i++)
    {
        printf("id=%d, len=%d, name=%s\n", bird_data[i].id, bird_data[i].len, bird_data[i].name);
    }
}

int compare_bird_id(const void *a, const void *b)
{

    bird *A = (bird *)a;
    bird *B = (bird *)b;

    return (A->id - B->id);
}

void read_bird_file(birds *data)
{
    FILE *bird_file;
    const char BIRD_FILE_NAME[] = "birds.bin";
    bird_file = fopen(BIRD_FILE_NAME, "rb");
    if (bird_file == NULL)
    {
        perror(BIRD_FILE_NAME);
        exit(EXIT_FAILURE);
    }

    int index = 0;
    int size = 0;
    size = size + 1;
    bird *bird_data = (bird *)malloc(sizeof(bird) * size);
    bird def_values = {0};

    *bird_data = def_values;
    while (1)
    {
        bird temp;
        fread(&temp, sizeof(bird), 1, bird_file);
        if (feof(bird_file))
        {
            break;
        }

        bird_data[index++] = temp;

        size++;
        bird *tmp = realloc(bird_data, sizeof(bird) * size);

        if (tmp == NULL)
        {
            perror("Reallocation failed\n");
            exit(EXIT_FAILURE);
        }
        else
        {
            bird_data = tmp;
        }
    }

    fclose(bird_file);
    qsort(bird_data, index, sizeof(bird), compare_bird_id);

    data->size = index;
    data->birds = bird_data;
}
