#include "include/main.h"
#include "include/bird_entry.h"
#include "include/bird.h"
#include "include/remove_non_common.h"
#include "include/write_file.h"
#include "include/menu.h"
#include "include/list_birds_in_a_year.h"
#include "include/show_bird_data_year_wise.h"
#include "include/show_bird_data_month_wise.h"
#include "include/delete_bird_observations.h"
#include "include/display_bird_sightings.h"

int main()
{
    birds data;
    data.size = 0;
    data.birds = NULL;
    read_bird_file(&data);
    // print_bird(data.birds, data.size);

    stats data1;
    data1.size = 0;
    data1.data = NULL;
    read_observation_file(&data1);

    int choice;

    do
    {
        printf("\n1. Filter Entries (Problem 1)\n");
        printf("2. Show Choices (Problem 2)\n");
        printf("3. Show birds seen in a year (Problem 3)\n");
        printf("4. Show birds seen in all years (Problem 4)\n");
        printf("5. Show birds month wise (Problem 5)\n");
        printf("6. Delete bird observations (Problem 6)\n");
        printf("7. Display bird sightings (Problem 7)\n");
        printf("8. Print birds.bin and observations.bin files\n");
        printf("9. Exit\n");

        printf("\nplease enter your choice: ");
        scanf("%d", &choice);
        getchar();

        printf("\n");
        switch (choice)
        {
        case 1:
            filter_entries(&data, &data1);
            printf("Filtered data\n");
            break;
        case 2:
            show_choices(data, &data1);
            break;
        case 3:
            show_birds_seen_in_a_year(data, data1);
            break;
        case 4:
            show_bird_seen_in_all_years(data, data1);
            break;
        case 5:
            show_birds_month_wise(data, data1);
            break;
        case 6:
            delete_bird_observations(data, &data1);
            break;
        case 7:
            display_bird_sightings(&data, &data1);
            break;
        case 8:
            print_bird(data.birds, data.size);
            print_stats(data1.data, data1.size);
            break;
        case 9:
            printf("Exiting the program.\n");
        }
        printf("\n");
    } while (choice != 9);

    // problem statement 1
    // filter_entries(&data, &data1);

    // problem statement 2
    // show_choices(data, &data1);

    // problem statement 3
    // show_birds_seen_in_a_year(data, data1);

    // problem statement 4
    // show_bird_seen_in_all_years(data, data1);

    // problem statement 5
    // show_birds_month_wise(data, data1);

    // problem statement 6
    // delete_bird_observations(data, &data1);

    // problem statement 7
    // display_bird_sightings(&data, &data1);

    free(data.birds);
    free(data1.data);
}
